var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

var reRef = new RegExp('<ref>(.*)</ref>');
var reReferences = new RegExp('<references/>');
var footnotes = [];
var notenumber = 0;

rl.on('line', (line) => {
    var result = reRef.exec(line);
    if (result) {
        var ref = '[^' + (++notenumber) + ']'
        console.log(line.replace(result[0], ref));
        footnotes.push(result[1]);
    } else if (reReferences.test(line)) {
        for (var i = 0; i < footnotes.length; i++) {
            console.log('[^%d]: %s', i + 1, footnotes[i]);
        }
    } else {
        console.log(line);
    }
    
});

rl.on('close', () => {
});
